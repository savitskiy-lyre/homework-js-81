import {CREATE_SHORT_LINK_FAILURE, CREATE_SHORT_LINK_REQUEST, CREATE_SHORT_LINK_SUCCESS} from "../actions/linksActions";

const initState = {
   currentLink: null,
   linkSubmitLoading: false,
   error: null,
};
export const linksReducer = (state = initState, action) => {
   switch (action.type) {
      case CREATE_SHORT_LINK_REQUEST:
         return {...state, linkSubmitLoading: true, error: null}
      case CREATE_SHORT_LINK_SUCCESS:
         return {...state, linkSubmitLoading: false, currentLink: action.payload}
      case CREATE_SHORT_LINK_FAILURE:
         return {...state, linkSubmitLoading: false, error: action.payload}

      default:
         return state;
   }
}