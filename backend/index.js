const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const shortUrl = require('./routers/shortUrl');
const links = require('./routers/links');

const server = express();
server.use(cors());
server.use(express.json());

const port = 7000;

server.use('/', shortUrl);
server.use('/links', links);

const run = async () => {
  await mongoose.connect('mongodb://localhost/linksStorage');

  server.listen(port, () => {
    console.log(`Server is started on ${port} port !`);
  })
}
run().catch(e => console.error(e));