import {LINKS_URL} from "../../config";
import {axiosApi} from "../../axiosApi";

export const CREATE_SHORT_LINK_REQUEST = 'CREATE_SHORT_LINK_REQUEST';
export const CREATE_SHORT_LINK_SUCCESS = 'CREATE_SHORT_LINK_SUCCESS';
export const CREATE_SHORT_LINK_FAILURE = 'CREATE_SHORT_LINK_FAILURE';

export const createShortLinkRequest = () => ({type: CREATE_SHORT_LINK_REQUEST});
export const createShortLinkSuccess = (data) => ({type: CREATE_SHORT_LINK_SUCCESS, payload: data});
export const createShortLinkFailure = (error) => ({type: CREATE_SHORT_LINK_FAILURE, payload: error});

export const createShortLink = (link) => {
   return async (dispatch) => {
      try {
         dispatch(createShortLinkRequest());
         const {data} = await axiosApi.post(LINKS_URL, link);
         dispatch(createShortLinkSuccess(data));
      } catch (error) {
         dispatch(createShortLinkFailure(error));
      }
   }
}