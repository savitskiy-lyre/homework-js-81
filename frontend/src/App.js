import {Box, Container, CssBaseline, Grid, Stack, TextField, Typography} from "@mui/material";
import {LoadingButton} from "@mui/lab";
import {useDispatch, useSelector} from "react-redux";
import {createShortLink} from "./store/actions/linksActions";
import {useState} from "react";
import {BASE_URL} from "./config";

const App = () => {
   const dispatch = useDispatch();
   const currentLink = useSelector(state => state.links.currentLink);
   const linkSubmitLoading = useSelector(state => state.links.linkSubmitLoading);
   const [linkInp, setLinkInp] = useState('');

   const onLinkSubmit = (e) => {
      e.preventDefault();
      dispatch(createShortLink({url: linkInp}))
        .then(() => {
           setLinkInp('');
        })
   }

   return (
     <Box sx={{background: '#c8eeee'}} minHeight={"100vh"}>
        <CssBaseline/>
        <Container>
           <Grid container minHeight={"100vh"} justifyContent={"center"} direction={'column'}
                 textAlign={"center"}>
              <Stack spacing={4}>
                 <Typography variant={"h3"}>
                    Shorten your link !
                 </Typography>
                 <Grid container direction={"column"} component={'form'} onSubmit={onLinkSubmit}>
                    <Grid item>
                       <TextField
                         value={linkInp}
                         onChange={(e) => setLinkInp(e.target.value)}
                         required
                         fullWidth
                         label={'Link'}
                         sx={{
                            background: 'white'
                         }}
                       />
                    </Grid>
                    <Grid item mt={5}>
                       <LoadingButton
                         type={'submit'}
                         loading={linkSubmitLoading}
                         variant={'contained'}
                         color={'secondary'}
                       >
                          Shorten
                       </LoadingButton>
                    </Grid>
                 </Grid>
                 {currentLink && (
                   <>
                      <Typography variant={"h5"}>
                         Your link now looks like this:
                      </Typography>
                      <Typography variant={"subtitle1"}>
                         <a href={BASE_URL + '/' + currentLink.shortUrl}>{BASE_URL + '/' + currentLink.shortUrl}</a>
                      </Typography>
                   </>
                 )}
              </Stack>
           </Grid>
        </Container>
     </Box>
   );
};

export default App;
