const express = require('express');
const LinkURL = require('../models/LinkURL');
const router = express.Router();

router.get('/:shortUrl', async (req, res) => {
if (!req.params.shortUrl) res.status(400).send({error: 'Data not valid'})
    try {
      const result = await LinkURL.find({shortUrl: req.params.shortUrl});
      if (result.length !== 0) {
        res.status(301).redirect(result[0].url)
      } else {
        res.status(404).send({error: 'Link not found'})
      }

    } catch (err) {
      res.sendStatus(500)
    }
  }
)

module.exports = router;