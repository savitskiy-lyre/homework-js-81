const mongoose = require('mongoose');
const { nanoid } = require('nanoid');
const LinkURLSchema = new mongoose.Schema({
  shortUrl: {
    type: String,
    default: () => nanoid(5)
  },
  url: {
    type: String,
    required: true,
  }
})

const LinkURL = mongoose.model('links', LinkURLSchema);

module.exports = LinkURL;