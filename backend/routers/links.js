const express = require('express');
const router = express.Router();
const LinkURL = require('../models/LinkURL');

router.post('/', async (req, res) => {
  if (!req.body.url) return res.status(400).send({error: 'Data not valid'})
  try {
    let link;
    let result;
    do {
      link = new LinkURL({url: req.body.url});
      result = await LinkURL.find({shortUrl: link.shortUrl});
    } while (result.length !== 0)
    await link.save();
    res.send(link)
  } catch (err) {
    res.sendStatus(500)
  }
});

module.exports = router;